import { FrameCard } from './FrameCard'

interface Props {
    frames: Frame[]
}
export function FrameList({ frames }: Props) {
    const done =
        !frames.some((frame) => frame.score === undefined) &&
        frames.length === 10
    return (
        <div className="flex flex-wrap mt-2 rounded bg-slate-100">
            {frames.length ? (
                frames.map((frame, index) => (
                    <FrameCard key={index} index={index} frame={frame} />
                ))
            ) : (
                <p className="p-4">No frames yet</p>
            )}
            {done && (
                <div className="frame-card tabular-nums ml-2 my-2">
                    <p className="header">Total Score</p>
                    <p className="p-2 text-lg font-semibold text-center">
                        {frames[frames.length - 1].score}
                    </p>
                </div>
            )}
        </div>
    )
}
