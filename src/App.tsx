import { useState } from 'react'
import { PinsInput } from './PinsInput'
import { FrameList } from './FrameList'
import { getFrames } from './getFrames'

function App() {
    const [games, setGames] = useState<Frame[][]>([])
    const [rolls, setRolls] = useState<number[]>([])
    const { frames, remaining } = getFrames(rolls)
    const done =
        !frames.some((frame) => frame.score === undefined) &&
        frames.length === 10

    return (
        <>
            <header className="flex justify-between items-center bg-slate-300 p-2">
                <h1 className="text-xl">Bowling Score</h1>
                <button
                    className="bg-white rounded border border-blue-500 px-2 py-1 disabled:opacity-50"
                    disabled={!frames.length}
                    onClick={() => {
                        if (
                            done ||
                            window.confirm(
                                'Games is unfinished, are you sure you want to create a new game?'
                            )
                        ) {
                            setGames([frames, ...games])
                            setRolls([])
                        }
                    }}
                >
                    New Game
                </button>
            </header>

            <main className="container mx-auto p-2">
                <PinsInput
                    max={remaining}
                    disabled={done}
                    onSubmit={(pins) => setRolls([...rolls, pins])}
                />
                <FrameList frames={frames} />

                {!!games.length && (
                    <p className="text-lg mt-2">Previous games</p>
                )}
                {games.map((frames, index) => (
                    <FrameList key={index} frames={frames} />
                ))}
            </main>
        </>
    )
}

export default App
