import { describe, expect, it } from 'vitest'
import { getFrames } from './getFrames'

describe('getFrames tests', () => {
    it('return empty frame on initial', () => {
        const rolls: number[] = []
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toHaveLength(0)
        expect(remaining).toEqual(10)
    })

    it('1 strike frame', () => {
        const rolls = [10]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: undefined,
                rolls: [10],
            },
        ])
        expect(remaining).toBe(10)
    })

    it('2 strike frame', () => {
        const rolls = [10, 10]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: undefined,
                rolls: [10],
            },
            {
                score: undefined,
                rolls: [10],
            },
        ])
        expect(remaining).toBe(10)
    })

    it('3 strike frame', () => {
        const rolls = [10, 10, 10]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: 30,
                rolls: [10],
            },
            {
                score: undefined,
                rolls: [10],
            },
            {
                score: undefined,
                rolls: [10],
            },
        ])
        expect(remaining).toBe(10)
    })

    it('1 non-strike roll', () => {
        const rolls = [6]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: undefined,
                rolls: [6],
            },
        ])
        expect(remaining).toBe(4)
    })

    it('1 spare frame', () => {
        const rolls = [6, 4]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: undefined,
                rolls: [6, 4],
            },
        ])
        expect(remaining).toBe(10)
    })

    it('1 spare frame and 1 non-strike roll', () => {
        const rolls = [6, 4, 7]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: 17,
                rolls: [6, 4],
            },
            {
                score: undefined,
                rolls: [7],
            },
        ])
        expect(remaining).toBe(3)
    })

    it('1 frame', () => {
        const rolls = [6, 3]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: 9,
                rolls: [6, 3],
            },
        ])
        expect(remaining).toBe(10)
    })

    it('1 frame, 1 non-strike roll', () => {
        const rolls = [6, 3, 4]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            {
                score: 9,
                rolls: [6, 3],
            },
            {
                score: undefined,
                rolls: [4],
            },
        ])
        expect(remaining).toBe(6)
    })

    it('perfect score', () => {
        const rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 30, rolls: [10] },
            { score: 60, rolls: [10] },
            { score: 90, rolls: [10] },
            { score: 120, rolls: [10] },
            { score: 150, rolls: [10] },
            { score: 180, rolls: [10] },
            { score: 210, rolls: [10] },
            { score: 240, rolls: [10] },
            { score: 270, rolls: [10] },
            { score: 300, rolls: [10, 10, 10] },
        ])
        expect(remaining).toBe(0)
    })

    it('last roll not a strike', () => {
        const rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 3]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 30, rolls: [10] },
            { score: 60, rolls: [10] },
            { score: 90, rolls: [10] },
            { score: 120, rolls: [10] },
            { score: 150, rolls: [10] },
            { score: 180, rolls: [10] },
            { score: 210, rolls: [10] },
            { score: 240, rolls: [10] },
            { score: 270, rolls: [10] },
            { score: 293, rolls: [10, 10, 3] },
        ])
        expect(remaining).toBe(7)
    })

    it('last frame a spare frame', () => {
        const rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 10]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 30, rolls: [10] },
            { score: 60, rolls: [10] },
            { score: 90, rolls: [10] },
            { score: 120, rolls: [10] },
            { score: 150, rolls: [10] },
            { score: 180, rolls: [10] },
            { score: 210, rolls: [10] },
            { score: 235, rolls: [10] },
            { score: 255, rolls: [10] },
            { score: 275, rolls: [5, 5, 10] },
        ])
        expect(remaining).toBe(0)
    })

    it('last frame just a frame', () => {
        const rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 1]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 30, rolls: [10] },
            { score: 60, rolls: [10] },
            { score: 90, rolls: [10] },
            { score: 120, rolls: [10] },
            { score: 150, rolls: [10] },
            { score: 180, rolls: [10] },
            { score: 210, rolls: [10] },
            { score: 235, rolls: [10] },
            { score: 251, rolls: [10] },
            { score: 257, rolls: [5, 1] },
        ])
        expect(remaining).toBe(4)
    })

    it('all zeros', () => {
        const rolls = [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
            { score: 0, rolls: [0, 0] },
        ])
        expect(remaining).toBe(10)
    })

    it('all 1', () => {
        const rolls = [
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        ]
        const { frames, remaining } = getFrames(rolls)
        expect(frames).toEqual([
            { score: 2, rolls: [1, 1] },
            { score: 4, rolls: [1, 1] },
            { score: 6, rolls: [1, 1] },
            { score: 8, rolls: [1, 1] },
            { score: 10, rolls: [1, 1] },
            { score: 12, rolls: [1, 1] },
            { score: 14, rolls: [1, 1] },
            { score: 16, rolls: [1, 1] },
            { score: 18, rolls: [1, 1] },
            { score: 20, rolls: [1, 1] },
        ])
        expect(remaining).toBe(8)
    })

    it('total rolls exceed', () => {
        expect(() =>
            getFrames([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
        ).toThrow('Number of rolls exceeded')
        expect(() =>
            getFrames([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 4, 1])
        ).toThrow('Number of rolls exceeded')
    })
})
