interface Props {
    index: number
    frame: Frame
}
export function FrameCard({ index, frame }: Props) {
    const pins1 = frame.rolls[0]
    const pins2 = frame.rolls.at(1)
    const pins3 = frame.rolls.at(2)
    let boxedText = ''

    if (pins2 !== undefined) {
        const total = pins1 + pins2
        if (total === 10) {
            boxedText = '/'
        } else {
            boxedText = pins2.toString()
        }
    } else if (pins1 === 10) {
        boxedText = 'X'
    }

    return (
        <div className="frame-card tabular-nums ml-2 my-2">
            <p className="header">{index + 1}</p>
            {index < 9 ? (
                <div className="flex">
                    <p className="px-4 py-1">{pins1 < 10 ? pins1 : ''}</p>
                    <p className="px-4 py-1 border-b border-l border-gray-500">
                        {boxedText}
                    </p>
                </div>
            ) : (
                <div className="flex">
                    <p className="px-4 py-1">{pins1 < 10 ? pins1 : 'X'}</p>
                    <p className="px-4 py-1 border-b border-l border-gray-500">
                        {pins2 ? (pins2 < 10 ? pins2 : 'X') : ''}
                    </p>
                    <p className="px-4 py-1 border-b border-l border-gray-500">
                        {pins3 ? (pins3 < 10 ? pins3 : 'X') : ''}
                    </p>
                </div>
            )}
            <p className="p-2 font-semibold text-center">
                {frame.score ?? '-'}
            </p>
        </div>
    )
}
