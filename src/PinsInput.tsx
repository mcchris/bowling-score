import { useRef } from 'react'

interface Props {
    max?: number
    disabled?: boolean
    onSubmit: (pins: number) => void
}
export function PinsInput(props: Props) {
    const inputEl = useRef<HTMLInputElement>(null)

    return (
        <form
            className="mt-2"
            onSubmit={(event) => {
                event.preventDefault()
                const input = inputEl.current
                if (!input) {
                    return
                }
                const pins = parseInt(input.value ?? '0', 10)
                props.onSubmit(pins)
                input.value = ''
            }}
        >
            <input
                ref={inputEl}
                className="rounded mr-1 mt-1 disabled:opacity-50"
                type="number"
                min="0"
                max={props.max ?? 10}
                required
                onClick={(event) => event.currentTarget.select()}
                disabled={props.disabled}
            />
            <button
                className="bg-blue-300 rounded border border-blue-500 p-2 mt-1 disabled:opacity-50"
                type="submit"
                disabled={props.disabled}
            >
                Submit
            </button>
        </form>
    )
}
