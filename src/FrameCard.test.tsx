import { render, screen } from '@testing-library/react'
import { describe, expect, it } from 'vitest'
import { FrameCard } from './FrameCard'

describe('FrameCard', () => {
    it('display strike frame', () => {
        render(
            <FrameCard
                index={4}
                frame={{
                    score: undefined,
                    rolls: [10],
                }}
            />
        )
        expect(screen.getByText(/5/)).toBeTruthy()
        expect(screen.getByText(/X/)).toBeTruthy()
    })

    it('display spare frame', () => {
        render(
            <FrameCard
                index={4}
                frame={{
                    score: undefined,
                    rolls: [5, 5],
                }}
            />
        )
        expect(screen.getByText(/\//)).toBeTruthy()
    })

    it('display a frame', () => {
        render(
            <FrameCard
                index={4}
                frame={{
                    score: undefined,
                    rolls: [5, 3],
                }}
            />
        )
        expect(screen.getByText(/3/)).toBeTruthy()
    })

    it('display a 10th frame', () => {
        render(
            <FrameCard
                index={9}
                frame={{
                    score: 30,
                    rolls: [10, 10, 10],
                }}
            />
        )
        expect(screen.getAllByText(/X/)).toHaveLength(3)
        expect(screen.getByText(/30/)).toBeTruthy()
    })
})
