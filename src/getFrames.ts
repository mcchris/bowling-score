export function getFrames(rolls: number[]): {
    frames: Frame[]
    remaining: number
} {
    const frames = new Array<Frame>()
    let prevFrame: Frame | undefined
    let currFrame: Frame = { score: undefined, rolls: [] }
    let remaining = 10

    rolls.forEach((pins, index) => {
        if (!frames.includes(currFrame)) {
            frames.push(currFrame)
        }
        currFrame.rolls.push(pins)
        const totalRolls = currFrame.rolls.length
        const prevFrameScore = prevFrame?.score ?? 0

        if (frames.length === 10) {
            if (totalRolls > 3) {
                throw new Error('Number of rolls exceeded')
            }
            const totalPins = currFrame.rolls.reduce(
                (total, pins) => total + pins,
                0
            )
            if ((totalRolls === 1 || totalRolls === 2) && totalPins === 10) {
                remaining = 10
            } else if (totalRolls === 2 && totalPins < 10) {
                currFrame.score = totalPins + prevFrameScore
                remaining = 10 - totalPins
            } else if (totalRolls === 3) {
                if (currFrame.rolls[0] + currFrame.rolls[1] < 10) {
                    throw new Error('Number of rolls exceeded')
                }
                currFrame.score = totalPins + prevFrameScore
                remaining = remaining - pins
            }
        } else {
            function reset() {
                prevFrame = currFrame
                currFrame = { score: undefined, rolls: [] }
                remaining = 10
            }

            const totalPins = currFrame.rolls.reduce(
                (total, pins) => total + pins,
                0
            )
            remaining = remaining - totalPins
            if (totalRolls === 1 && totalPins === 10) {
                const nextRoll1 = rolls[index + 1]
                const nextRoll2 = rolls[index + 2]
                if (nextRoll1 !== undefined && nextRoll2 !== undefined) {
                    currFrame.score =
                        totalPins + nextRoll1 + nextRoll2 + prevFrameScore
                }
                reset()
            } else if (totalRolls === 2 && totalPins === 10) {
                const nextRoll = rolls[index + 1]
                if (nextRoll !== undefined) {
                    currFrame.score = totalPins + nextRoll + prevFrameScore
                }
                reset()
            } else if (totalRolls === 2) {
                currFrame.score = totalPins + prevFrameScore
                reset()
            }
        }
    })
    return { frames, remaining }
}
